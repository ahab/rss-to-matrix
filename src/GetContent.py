#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=================================================================================================
# Description:
"""
This script parse some rss-feeds. They should be given in the "url-list". After parsing the url,
they would be sent with matrix. Feedback and comments are highly appreciated.
"""

# Date: 20211129
# Revision: 20240213
__author__ = "code@schoeneberge.eu"
__version__ = "0.0.9"
#=================================================================================================

import os
import datetime
import hashlib
from dotenv import load_dotenv
import asyncio
import requests
import time
import yaml
import dbservice
from requests.exceptions import RequestException, HTTPError, Timeout, TooManyRedirects
from bs4 import BeautifulSoup
from nio import AsyncClient

CONF_FOLDER_PATH = pathlib.Path(f"{os.getenv('HOME')}/.config/rssfeeder")
CONF_FILE_NAME = "config.yaml"
CONF_FILE_PATH = CONF_FOLDER_PATH / CONF_FILE_NAME

with CONF_FILE_PATH.open('r', encoding='UTF-8') as the_config:
    cfg = yaml.load(the_config, Loader=yaml.FullLoader)

async def sendmatrix(domain, user, passwd, matrix_room, sendfile):
    client = AsyncClient(domain, user)

    await client.login(passwd)
    await client.room_send(
        room_id= matrix_room,
        message_type="m.room.message",
        content={
            "msgtype": "m.text",
            "body": send_file
        }
    )
    await client.close()


def get_rss(url, matrix_room, rss_database):
    """
    This function parses a page and looks for the title, the pubDate, the link and, if
    available, a description and/or an abstract.
    """
    try:
        response = requests.get(url, timeout=5)
        send_file = ""
        soup = BeautifulSoup(response.content, features='xml')
        articles = soup.findAll('item')

# Parse over the site and find title, link, pub-date and, if there is one, abstract
        for item in articles:
            title = item.find('title').text
            link = item.find('link').text
            try:
                published = item.find('pubDate').text
            except AttributeError:
                published = datetime.datetime.now()
            try:
                abstract = item.find('abstract').text
                texthash_for_sql = hashlib.md5(abstract.encode('UTF-8')).hexdigest()
            except AttributeError:
                texthash_for_sql = None
                abstract = None
            try:
                description = item.find('description').text
                texthash_for_sql = hashlib.md5(description.encode('UTF-8')).hexdigest()
            except AttributeError:
                texthash_for_sql = None
                description = None

            if texthash_for_sql is None:
                texthash_for_sql = hashlib.md5(title.encode('UTF-8')).hexdigest()


# Set Send-Time
            now = datetime.datetime.now()
# The format should be e. g.: Tue, 13 Jun 2023 20:30:40 +0200
            time_now = now.strftime('%a, %d %b %Y %H:%M:%S %z')

# Check if database existing
            if not os.path.isfile(rss_database):
                dbservice.create_table(database=rss_database)

# Check if title already exists
            existing_pubdate = dbservice.get_data(database=rss_database, var1="published")
            if published not in existing_pubdate:
                existing_hash = dbservice.get_data(database=rss_database, var1="texthash")
                if texthash_for_sql not in existing_hash:
                    dbservice.insert_data(database=rss_database, texthash=texthash_for_sql, \
                        pubdate=published, senddate=time_now)

# Create and fill the send_file
                    send_file += f"{title}\n"
                    if abstract:
                        send_file += f"{abstract}\n"
                    if description:
                        send_file += f"{description}\n"
                    send_file += f"{link}\n\n"

# Send files via matrix

        if send_file != "":
            asyncio.run(sendmatrix(cfg['FQDN_DOMAINS'], cfg['USER']['MATRIX'], \
                cfg['PASS']['MATRIX'], matrix_room, send_file))

    except (ConnectionError, HTTPError, Timeout, TooManyRedirects, RequestException) \
        as request_error:
        print("The scraping job failed. See exception: ", request_error)

if __name__ == "__main__":
        for each_page in cfg['PAGE']:
            get_rss(each_page['URL'], each_page['ROOM'], each_page['DB'])
            time.sleep(5)
