#!/usr/bin/env python3
# -*- coding: utf8 -*-

__date__ = "24.09.2023"

import sys
import sqlite3
import getopt

# Create database:
def create_table(database):
    conn = sqlite3.connect(database)
    conn.execute('''CREATE TABLE MYRSSTABLE
            (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            TEXTHASH   TEXT    NOT NULL,
            PUBLISHED_DATE  TEXT    NOT NULL,
            SEND_DATE   TEXT);''')
    conn.close()

# Insert some data:
def insert_data(database, texthash, pubdate, senddate):
    conn = sqlite3.connect(database)
    conn.execute("INSERT INTO MYRSSTABLE (TEXTHASH, PUBLISHED_DATE, SEND_DATE) VALUES (?, ?, ?);", (texthash, pubdate, senddate))
    conn.commit()
    print("Records created successfully")
    conn.close()

def get_data(database, var1):
    """
    This function returns the respective value on a sql query. Possible values are
    title, texthash, pubDate and sendDate
    """
    return_file = []
    conn = sqlite3.connect(database)
    cursor = conn.execute("SELECT id, texthash, published_date, send_date from MYRSSTABLE")
    if var1 == "id":
        for row in cursor:
            return row[0]
    elif var1 == "texthash":
        for row in cursor:
            return_file.append(row[1])
    elif var1 == "published":
        for row in cursor:
            return row[2]
    elif var1 == "senddate":
        for row in cursor:
            return row[3]
    conn.close()
    return return_file
