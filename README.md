# RSS-Feed-Reader/Sender #

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

Last updated on: 20230812

## Table of contents ##

1. [Description](#description)
2. [Prerequisites](#prerequisites)
3. [Usage](#usage)
4. [Troubleshooting](#troubleshooting)

## Description ##

This script reads the rss-feed of one (or more) certain specified pages and then sends it via matrix (or print it to stdout). To avoid getting messages twice, the entries, or more precisely a hash (md5) of the abstract/description (or, if both are missing, of the title) are then written to a sqlite database together with the timestamp of creation and the timestamp of retrieval. Further data is then matched with this.
The main script is moved to the `/usr/local/sbin/` folder during installation.

## Run local as python-script ##

### Prerequisites ###

This script requires the **python modules** contained in the requirements.txt. This requires python3.x and pip and a virtual environment. The environment could be created with `pyton3 -m venv .` (Note the dot) in the rss-folder and then activate it: `source bin/activate`. The modules can be installed with `pip install -r requirements.txt`. You also need a room (room ID) where the messages can be received and the credentials of a user who should send the messages.

### Usage ###

1. Clone the repo.
2. You also need a **matrix user account** with **password** and the **room ID** of the matrix room where the messages should be posted.
3. Run the `confighelper.py` script: `python3 rss-to-matrix/src/confighelper.py` and enter the required data. The script creates the folders `/var/lib/rssfeeder/` for the database and `$HOME/.config/rssfeeder/` for the config (config.yaml) automatically.
4. Now additional URLs can be included in the `config.yaml`.
5. That's all. Now the script can be executed with `python3 /usr/local/sbin/GetContent.py`.
6. To be able to receive messages regularly a `systemd-timer` or a `cron`-job should be created. For examples see examples folder.

If you want to create the matrix-script yourself, there is a tutorial [here](https://matrix.org/docs/guides/usage-of-matrix-nio#use-room_send)

## Run as docker container ##

1. Copy the example config at the recent directory and adjust it
2. Build the docker container: `docker build -t rssfeeder:<version> .` (Note the dot at the end)

## Troubleshooting ##

Error: `No module named 'dbservice'`

```python
Traceback (most recent call last):
  File "/usr/local/sbin/GetContent.py", line 21, in <module>
    import dbservice
ModuleNotFoundError: No module named 'dbservice'
```

Solution: Make sure, that there is a `__init__.py` file in the folder of `dbservice.py`

Feedback and comments are highly appreciated
