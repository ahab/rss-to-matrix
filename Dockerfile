FROM debian:bookworm

WORKDIR /root

RUN apt-get update
RUN apt-get install vim python3 python3-pip -y

ADD src/dbservice.py /usr/local/sbin/dbservice.py
RUN touch /usr/local/sbin/__init__.py
ADD requirements.txt .
ADD src/GetContent.py /usr/local/sbin/GetContent
RUN chmod 0700 /usr/local/sbin/GetContent

RUN pip install --break-system-packages -r requirements.txt

RUN mkdir -p /root/.config/rssfeeder
RUN mkdir -p /var/lib/rssfeeder

#ENTRYPOINT ["/usr/local/sbin/GetContent"]
