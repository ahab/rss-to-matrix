# Changelog

## Version 0.0.9

20240213

- Adjust the example.yaml in the new style with url, room and db in one section
- Simplify the exception handling for the request section
- Insert a try block for published and if no published item were found, set published to datetime.now()
- Move reading of config to the bottom and make it runable with more than one url
- It is now possible to match url - matrix_rooms and databases, see exampleconf.yaml
- Insert a delay of 5 seconds to avoid ratelimiting from the matrix server
- Insert yaml second time, only with this we can relate url, matrix_rooms and databases

## Version 0.0.8

20240204

- Make testing easier, therefor delete the config file and realize this via .env file.
- Remove `confighelper.py`
- Update README.md, usage of confighelper.py is now depricated
- Add example .env file
- Remove exampleconf.yaml from example folder

## Version 0.0.7

20230812

- For improving speed add a test if published date exists in database bevore testing if there is the texthash in the database.
- Add a Dockerfile for testing and for production. The container is based on debian bookworm and needs a config.yaml via mount when running: `docker run -it --mount type=bind,source=${pwd}config.yaml,target=/root/.config/rssfeeder/config.yaml,readonly <docker-image> /bin/bash` or, a working config.yaml sourced from the example config.
- Update the README.md. Now includes a usage section for docker
- Remove unneeded functions from dbservice script

## Version 0.0.6

- Separate the program parts that do things in the database. Rename the database file from MySqlite.py to dbservice.py.
